/*
 * Copyright (C) 2020 Sklave Daniel
 * This file is part of BDSMLR Favorites <https://gitlab.com/SklaveDaniel/bdsmlr-favorites/-/tree/master>.
 *
 * BDSMLR Favorites is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BDSMLR Favorites is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BDSMLR Favorites.  If not, see <http://www.gnu.org/licenses/>.
 */
package io.gitlab.sklavedaniel.bdsmlrfavorites

import java.io.{BufferedWriter, File, FileWriter}

import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import scala.annotation.tailrec
import scala.util.Using
import scopt.OParser

object Main {

  case class Config(email: String = "", password: String = "", start: Int = 1, count: Int = Integer.MAX_VALUE, images: Option[File] = None, videos: Option[File] = None)

  val builder = OParser.builder[Config]
  val parser = {
    import builder._
    OParser.sequence(
      programName("bdsmlrfav"),
      head("BDMSLR Favorites", "0.0.1"),
      opt[Int]('s', "start").valueName("<number>").text("page to start at, 1 for first").action((x, c) => c.copy(start = x)),
      opt[Int]('c', "count").valueName("<number>").text("number of pages to process").action((x, c) => c.copy(count = x)),
      opt[File]('i', "images").valueName("<file>").text("output file for image urls").action((x, c) => c.copy(images = Some(x))),
      opt[File]('v', "videos").valueName("<file>").text("output file for video urls").action((x, c) => c.copy(videos = Some(x))),
      arg[String]("<email>").text("bdsmlr login email").action((x, c) => c.copy(email = x)),
      arg[String]("<password>").text("bdsmlr login password").action((x, c) => c.copy(password = x))
    )
  }

  def main(args: Array[String]): Unit = {
    OParser.parse(parser, args, Config()) match {
      case Some(config) => collectFavorites(config)
      case _ =>
    }
  }

  def collectFavorites(config: Config): Unit = {
    val browser = JsoupBrowser()
    val login = browser.get("https://bdsmlr.com/login")
    val token = (login >> element("""input[name="_token"]""")).attrs("value")
    val loggedin = browser.post("https://bdsmlr.com/login", Map("_token" -> token, "email" -> config.email, "password" -> config.password))

    if ((loggedin >> elements(".has-error")).isEmpty) {
      (loggedin >?> text(".nameblogholder")).foreach(s => println(s"User name: $s"))

      Using.Manager { use =>
        val imglog = use(new BufferedWriter(new FileWriter(config.images.getOrElse(new File("images.txt")))))
        val vidlog = use(new BufferedWriter(new FileWriter(config.videos.getOrElse(new File("videos.txt")))))

        processLikes("https://bdsmlr.com/likes?page=" + config.start, config.count)

        @tailrec
        def processLikes(uri: String, count: Int): Unit = if (count > 0) {
          println("Processing " + uri)
          val likes = browser.get(uri)
          val imgs = (likes >> elements(".image_content img")).map(_.attr("src")).toList
          val vids = (likes >> elements(".imagev_content video source")).map(_.attr("src")).toList
          imglog.write(imgs.map(_ + "\n").mkString(""))
          vidlog.write(vids.map(_ + "\n").mkString(""))

          val next = (likes >> elements("""a[rel="next"]""")).headOption.flatMap(_.attrs.get("href"))
          if (next.nonEmpty) {
            processLikes(next.get, count - 1)
          }
        }
      }
    } else {
      System.err.println("Failed to login!")
    }
  }
}
