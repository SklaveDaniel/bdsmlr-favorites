name := "BDSMLR Favorites"

version := "0.0.1"

scalaVersion := "2.13.3"

scalacOptions ++= Seq("-unchecked", "-deprecation", "-Xcheckinit", "-encoding", "utf8")

libraryDependencies ++= Seq(
  "net.ruippeixotog" %% "scala-scraper" % "2.2.0",
  "com.github.scopt" %% "scopt" % "4.0.0-RC2"
)

fork := true

mainClass in run := Some("io.gitlab.sklavedaniel.bdsmlrfavorites.Main")

mainClass in assembly := Some("io.gitlab.sklavedaniel.bdsmlrfavorites.Main")


cancelable in Global := true

assemblyJarName in assembly := "bdsmlr-favorites.jar"
