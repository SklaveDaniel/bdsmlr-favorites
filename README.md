# BDSMLR Favorites

A simple tool that processes all your favorite BDSMLR posts and dumps the URLs of all images and videos in two text files.
These files can be used to download the images or videos using e.g. `ẁget -nc -i images.txt`.
Run the tool with `java -jar bdmslrfavorites.jar` to see the command line options.

# Download

Latest version: [bdsmlr-favorites.jar](https://gitlab.com/SklaveDaniel/bdsmlr-favorites/-/jobs/artifacts/master/raw/target/scala-2.13/bdsmlr-favorites.jar?job=build)

# License

BDSMLR Favorites is licensed unter GPL 3. It is not a lot of code, but it should be free as in freedom to watch porn.
